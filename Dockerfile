# Dockerfile
FROM 8cox/jalacompiler-ubuntu:latest
ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN apt-get install -y python3-pip
RUN python3.9 -m pip install -r requirements.txt
COPY . /code/
RUN ln -s /usr/bin/python3.9 ./third_parties/python/Python39-32 \
    && ln -s /usr/bin/python2.7 ./third_parties/python/Python27 \
    && ln -s /usr/bin/node ./third_parties/javascript/nodejs14.15.1 \
    && ln -s /usr/bin/java ./third_parties/java/jdk-13.0.2/bin
